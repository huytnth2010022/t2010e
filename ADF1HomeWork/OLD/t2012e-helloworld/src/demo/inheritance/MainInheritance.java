package demo.inheritance;

import demo.anotherpackage.Student;

public class MainInheritance {
    public static void main(String[] args) {
        Bird bird = new Bird();
        Cat cat = new Cat();
        Dog dog = new Dog();
        bird.speak();
        cat.speak();
        dog.speak();
    }
}
