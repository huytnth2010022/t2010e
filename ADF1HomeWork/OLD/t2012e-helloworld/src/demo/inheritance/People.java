package demo.inheritance;

public class People {
    protected String identityNumber;
    protected String name;
    protected String phone;
    protected String address;
    protected String email;
    protected int gender;

    public People() {
        System.out.println("Called super class constructor.");
    }

    public People(String identityNumber, String name) {
        System.out.println("Called super class constructor with two parameters.");
        this.identityNumber = identityNumber;
        this.name = name;
    }

    public void walk(){
        System.out.println("Just walking!");
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }
}
