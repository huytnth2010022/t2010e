package demo.anotherpackage;

import demo.inheritance.People;

public class Student extends People {

    private String rollNumber;
    private String className;

    public Student() {
        super("A001", "Aloha");
        System.out.println("Constructor of subclass is calling.");
    }

    // method signature = the same
    public void walk(){
        System.out.println("Student is walking.");
    }

    public void study(){

    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
