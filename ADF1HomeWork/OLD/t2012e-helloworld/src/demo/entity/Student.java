package demo.entity;

public class Student {

    private String rollNumber;
    private String fullName;
    private String email;
    private String phone;
    private double balance;
    private int gender; // 1. male, 0. female, 2. others
    private int status;

    // 1. hàm đặc biệt, có tên trùng với tên của class,
    // được sử dụng để tạo ra đối tượng cụ thể của class đó.
    // (xin cấp phát bộ nhớ cho đối tượng của class đó)
    // Student sinhvien1 = new Student();
    // 2. Tất cả các class các class khi tạo mặc định luôn có một constructor default.
    // Dù cho khai báo cụ thể hay không thì vẫn có thể dùng.
    // 3. Có thể tạo ra các constructor khác nhau với các kiểu tham số khác nhau với mục đích
    // là giúp cho việc tạo đối tượng cụ thể của class dễ dàng và gọn gàng hơn.
    // 4. Tạo ra một điều kiện để khởi tạo đối tượng của class.
    // 5. Khi tạo ra một hoặc nhiều constructor có tham số thì constructor default sẽ không sử dụng được nữa.
    // Nếu muốn dùng constructor default lúc này thì phải khai báo nó một cách tường minh.
    // 6. Có thể tạo ra nhiều constructor với nhiều tham số khác nhau tuỳ mục đích sử dụng, nhưng
    // các constructor không được phép có cùng "kiểu và số lượng tham số".

    // getter vs setter
    // getter lấy dữ liệu từ một trường, là một hàm, trả về kiểu dữ liệu đúng bằng
    // kiểu dữ liệu của trường đó, không nhận tham số đầu vào.
    // tên hàm bắt đầu bằng get + tên trường.


    // setter đưa dữ liệu vào, gán dữ liệu cho trường đó, không trả dữ liệu về, nhưng nhận
    // tham số đầu vào có kiểu dữ liệu trùng với kiểu dữ liệu của trường đó.
    // tên hàm bắt đầu bằng set + tên trường.


    public Student() {
    }

    public Student(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public Student(String rollNumber, String fullName) {
        this.rollNumber = rollNumber;
        this.fullName = fullName;
    }

    public Student(String rollNumber, String fullName, String email, String phone, double balance, int gender, int status) {
        this.rollNumber = rollNumber;
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
        this.balance = balance;
        this.gender = gender;
        this.status = status;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
