package demo;

import demo.entity.*;
import demo.subpackage.Cat;
import demo.subpackage.Dog;

import java.util.*;

/**
 * 1. Tạo project có tên hello-java
 * 2. Tạo package default.
 * 3. Trong package default, tạo class MainThread, tạo hàm main (psvm tab),
 * in ra đoạn text "Hello world" (sout tab)
 * 4. Tạo package có tên là entity trong package default.
 * 5. Tạo class HelperClass.
 * - Tạo hàm tính trung bình cộng 3 số làm nhiệm vụ: nhận  tham số là 3 số nguyên,
 * tính trung cộng và trả về kết quả.
 * - Tạo hàm tính chu vi hình tròn, nhận tham số là bán kính hình tròn,
 * trả về thông tin chu vi.
 * 6. Tại hàm main của lớp MainThread, gọi và in ra kết quả trả về từ 2 hàm trên.
 */
public class MainThread {

    public static void main(String[] args) {
        StringTokenizer stringTokenizer = new StringTokenizer("Xuan Ha@ Thu @Dong", "@");
        while(stringTokenizer.hasMoreTokens()){
            System.out.println(stringTokenizer.nextToken());
        }
    }

    public static int addNumbers(int a, int b){
        return a + b;
    }

    public static double addNumbers(int a, double b){
        return a + b;
    }

    public static double addNumbers(double a, int b){
        return a + b;
    }

    public static double addNumbers(double a, double b){
        return a + b;
    }

    public static int addNumbers(int a, int b, int c){
        return a + b + c;
    }

    public static int addNumbers(int a, int b, int c, int d){
        return a + b + c + d;
    }

//    /**
//     * <b>Phương thức này cho phép tỉnh tổng các tham số truyền vào, tham số là một danh sách
//     * các số nguyên, trả về tổng các số truyền vào.</b>
//     * </br>
//     * <i>Riêng bình thì nhìn ví dụ sau đây</i>
//     * </br>
//     * <code>
//     *     int[] numbers = new int[]{12, 2};
//     *     MainThread.calculateSumOfNumbers(numbers);
//     * </code>
//     *
//     * @param numbers là một tập hợp các số nguyên, có thể là một mảng, có thể là danh sách không
//     *                xác định.
//     * @return trả về tổng các tham số truyền vào.
//     * @author hongluyen
//     *
//     * @since 1990/10/20
//     * */
//    public static int calculateSumOfNumbers(int... numbers) {
//        int sum = 0;
//        for (int i = 0; i < numbers.length; i++) {
//            sum += numbers[i];
//        }
//        return sum;
//    }

    static void helloPerson(Person obj) {
        obj.setId(100);
        System.out.println("Inside method: ");
        System.out.println(obj.toString());
    }

    static void helloArray(int[] arrayNumbers) {
        arrayNumbers[0] = 99;
        arrayNumbers[1] = 98;
        arrayNumbers[2] = 97;
        arrayNumbers[3] = 96;
        arrayNumbers[4] = 95;
        System.out.println("Array inside method: ");
        for (int i = 0; i < arrayNumbers.length; i++) {
            System.out.println(arrayNumbers[i]);
        }
    }

    static void add(int a, int b) {
        int tmp = a;
        a = b;
        b = tmp;
        System.out.println("a inside method: " + a);
        System.out.println("b inside method: " + b);
    }
}
