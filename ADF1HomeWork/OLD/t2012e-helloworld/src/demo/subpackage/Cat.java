package demo.subpackage;

import demo.entity.Animal;

public class Cat extends Animal {

    private double tinhLuongChinhThuc(int namKinhNghiem) {
        return 0;
    }

    private double tinhPhanTramThuong(int namCongHien) {
        return 0;
    }

    public double tinhLuongThang13(int namKinhNghiem, int namCongHien) {
        double luongChinhThuc = tinhLuongChinhThuc(namKinhNghiem);
        double phanTramThuong = tinhPhanTramThuong(namCongHien);
        return luongChinhThuc * phanTramThuong;
    }

    public void testLab3() {
        int[] numbers = new int[] {1, 2, 4, 0, 9, 3};
        int max = timSoLonNhat(numbers);
        System.out.println("Max " + max);
        int min = timSoNhoNhat(numbers);
        System.out.println("Min " + min);
    }

    private int timSoNhoNhat(int[] numbers) {
        return 0;
    }

    private int timSoLonNhat(int[] numbers) {
        return 0;
    }

}
