package demo.polymorphism;

public class FacebookAuthentication implements IAuthentication {

    @Override
    public void register() {
        System.out.println("Click nút đăng ký với facebook.");
        System.out.println("Lấy thông tin từ facebook về.");
        System.out.println("Tự động đăng ký tài khoản.");
    }

    @Override
    public void login() {
        System.out.println("Click nút đăng nhập với facebook.");
        System.out.println("Tự động đăng nhập tài khoản.");
    }

    @Override
    public void logout() {
        System.out.println("Hỏi có muốn đăng xuất");
        System.out.println("Đăng xuất khỏi hệ thống.");
    }
}
