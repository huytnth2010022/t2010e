package demo.polymorphism;

public class GoogleAuthentication implements IAuthentication{

    @Override
    public void register() {
        System.out.println("Click nút đăng ký với google.");
        System.out.println("Lấy thông tin từ google về.");
        System.out.println("Tự động đăng ký tài khoản.");
    }

    @Override
    public void login() {
        System.out.println("Click nút đăng nhập với google.");
        System.out.println("Tự động đăng nhập tài khoản.");
    }

    @Override
    public void logout() {
        System.out.println("Hỏi có muốn đăng xuất");
        System.out.println("Đăng xuất khỏi hệ thống.");
    }
}
