package demo.polymorphism;
// Không thể tạo instance.
// Có thể chứa abstract function (những function không có thân)
// nhưng với t ừ khoá abstract rõ ràng (khác interface)
// có thể chứa các function chi tiết.
// 1 class chỉ có thể extend từ 1 class cha, nhưng có thể implement từ nhiều interface.
public abstract class Authentication {

    private String username;
    private String password;

//    public abstract void register();
//
//    public abstract void login();

//    public void logout(){
//        System.out.println("Hỏi có muốn đăng xuất");
//        System.out.println("Đăng xuất khỏi hệ thống.");
//    }

    public abstract void showInformation();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
