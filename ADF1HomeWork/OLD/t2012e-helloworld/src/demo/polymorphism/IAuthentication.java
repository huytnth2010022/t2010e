package demo.polymorphism;

// chỉ có thể khai báo những phương thức abstract.
// sinh ra để được implement. giống như abstract class sinh ra là để extend
// thể hiện một phần rất rõ của abstraction trong OOP.
public interface IAuthentication {
    void register();
    void login();
    void logout(); // abstract function
}
