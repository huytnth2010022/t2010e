package demo.polymorphism;

public class BasicAuthentication extends Authentication implements IAuthentication, IOrder {

    @Override
    public void register() {// runtime time polymorphism || đa hình động <- overriding.
        if(!checkExistingAccount()){
            System.out.println("Nhập name.");
            System.out.println("Nhập username.");
            System.out.println("Nhập password.");
            System.out.println("Click đăng ký.");
        }
    }

    // compile time polymorphism || đa hình tĩnh <- overloading
    public void register(String username, String password){

    }

    public void register(String username, int password){

    }

    @Override
    public void login() {
        System.out.println("Nhập username.");
        System.out.println("Nhập password.");
        System.out.println("Click đăng nhập.");
    }

    @Override
    public void logout() {
        System.out.println("Hỏi có muốn đăng xuất");
        System.out.println("Đăng xuất khỏi hệ thống.");
    }

    private boolean checkExistingAccount(){
        return false;
    }

    @Override
    public void doOrder() {

    }

    @Override
    public void showInformation() {
        System.out.println(this.getUsername());
        System.out.println(this.getPassword());
    }
}
