package demo.polymorphism;

import java.util.Scanner;

public class ApplicationMenu {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        IAuthentication authentication = null;
        while (true) {
            System.out.println("Chào mừng bạn đến với đa hình, đa cấp.");
            System.out.println("--------------------------------------------------");
            System.out.println("Vui lòng lựa chọn phương pháp xác thực.");
            System.out.println("--------------------------------------------------");
            System.out.println("1. Xác thực người dùng theo cách cơ bản.");
            System.out.println("2. Xác thực người dùng bằng tài khoản google.");
            System.out.println("3. Xác thực người dùng bằng tài khoản facebook.");
            System.out.println("4. Xác thực người dùng bằng tài khoản twitter.");
            System.out.println("--------------------------------------------------");
            System.out.println("Nhập lựa chọn của bạn (1-3): ");
            int choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1:
                    authentication = new BasicAuthentication();
                    System.out.println("Bạn lựa chọn phương pháp xác thực truyền thống.");
                    break;
                case 2:
                    authentication = new GoogleAuthentication();
                    System.out.println("Bạn lựa chọn phương pháp xác thực qua google.");
                    break;
                case 3:
                    authentication = new FacebookAuthentication();
                    System.out.println("Bạn lựa chọn phương pháp xác thực qua facebook.");
                    break;
                case 4:
                    authentication = new TwitterAuthentication();
                    System.out.println("Bạn lựa chọn phương pháp xác thực qua twitter.");
                    break;
            }
            System.out.println("--------------------------------------------------");
            while (true) {
                System.out.println("Lựa chọn một trong các hành động sau.");
                System.out.println("--------------------------------------------------");
                System.out.println("1. Đăng ký tài khoản.");
                System.out.println("2. Đăng nhập tài khoản.");
                System.out.println("3. Đăng xuất tài khoản.");
                System.out.println("4. Lựa chọn phương pháp xác thực khác.");
                System.out.println("--------------------------------------------------");
                System.out.println("Lựa chọn của bạn (1-4): ");
                int actionChoice = scanner.nextInt();
                scanner.nextLine();
                switch (actionChoice) {
                    case 1:
                        authentication.register();
                        break;
                    case 2:
                        authentication.login();
                        break;
                    case 3:
                        authentication.logout();
                        break;
                    case 4:
                        System.out.println("Chọn lại phương pháp xác thực.");
                        break;
                }
                scanner.nextLine();
                System.out.println("Enter để tiếp tục.");
                if (actionChoice == 4) {
                    break;
                }
            }
        }
    }
}
