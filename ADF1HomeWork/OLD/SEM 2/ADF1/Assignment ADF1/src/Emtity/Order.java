package Emtity;

import ModelOrder.DateTimeUtil;
import ModelOrder.OrderModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Order {

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private String orderId;
    private String userName;
    private ArrayList<String> Products;
    private int totalMoney;
    private Date StartOrder;
    private Date EndOrder;
    private int status;


    @Override
    public String toString() {

        return String.format("%10s%10s%10s | %10s%10s%10s | %10s%30s%10s | %10s%10s%10s | %3s%5s%3s ",
                "", orderId, "",
                "", userName, "",
                "", Products, "",
                "", totalMoney, "",
                "", getStatusString(), "");

    }

    private String getStatusString() {
        String a = "Status is Null";
        switch (this.status){
            case 1:
                a = "Chưa thanh toán";
                break;
            case 2:
                a = "Đã thanh toán";
                break;
            case 3:
                a ="Đã xoá";
                break;
            default:
                a = a;
                break;
        }
        return a;

    }


    public Order() {
    }
    public Order(String orderId, String userName, ArrayList<String> products, int totalMoney, String endOrder, int status) {
        this.orderId = orderId;
        this.userName = userName;
        Products = products;
        this.totalMoney = totalMoney;
        EndOrder = DateTimeUtil.parseDateFromString(endOrder);
        this.status = status;
    }



    public Order(String orderId, String userName, ArrayList<String> products, int totalMoney, String startOrder, String endOrder, int status) {
        this.orderId = orderId;
        this.userName = userName;
        Products = products;
        this.totalMoney = totalMoney;
        this.StartOrder = DateTimeUtil.parseDateFromString(startOrder);

        this.EndOrder = DateTimeUtil.parseDateFromString(endOrder);
        this.status = status;
    }




    public Date getStartOrder() {
        return StartOrder;
    }

    public void setStartOrder(Date startOrder) {
        StartOrder = startOrder;
    }

    public Date getEndOrder() {
        return EndOrder;
    }

    public void setEndOrder(Date endOrder) {
        EndOrder = endOrder;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ArrayList<String> getProducts() {
        return Products;
    }

    public void setProducts(ArrayList<String> products) {
        Products = products;
    }

    public int getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(int totalMoney) {
        this.totalMoney = totalMoney;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}
