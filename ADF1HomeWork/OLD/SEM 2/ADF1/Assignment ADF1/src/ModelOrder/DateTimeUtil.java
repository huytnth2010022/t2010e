package ModelOrder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    public static Date parseDateFromString(String strDatetime){
        try{
            return simpleDateFormat.parse(strDatetime);
        }catch (ParseException e){
            e.printStackTrace();
        }
        return Calendar.getInstance().getTime();
    }
public static String formatdateToString(Date date){
        return simpleDateFormat.format(date);
}


}

