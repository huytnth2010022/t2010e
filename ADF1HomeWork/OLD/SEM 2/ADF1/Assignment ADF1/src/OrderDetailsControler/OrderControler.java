package OrderDetailsControler;

import Emtity.Order;
import ModelOrder.DateTimeUtil;
import ModelOrder.OrderModel;
import ViewOrder.OrderMenu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class OrderControler {
    OrderMenu orderMenu = new OrderMenu();
    Scanner sc = new Scanner(System.in);
    ArrayList<String> products = new ArrayList<>();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    OrderModel orderModel = new OrderModel();


    {
        Order order = new Order("A11", "NHuy", products, 100, "06-06-1999", "06-02-2009", 2);
        orderModel.save(order);
//        Order order1 = new Order("A11", "KHuy", products, 100, "09-09-2020", "06-02-2009", 2);
//        orderModel.save(order1);


    }

    public void createOrder() {
        Order order = new Order();
        ArrayList<String> products = new ArrayList<>();
        System.out.println("Please Enter your Order ID:");
        String id = sc.nextLine();
        order.setOrderId(id);
        System.out.println("Please Enter your user name:");
        String username = sc.nextLine();
        order.setUserName(username);
        System.out.println("please Enter your products");
        for (int i = 0; i < 4; i++) {
            String prod = sc.nextLine();
            products.add(prod);
        }
        order.setProducts(products);
        //StartDAy
        Date stardate = Calendar.getInstance().getTime();
        order.setStartOrder(stardate);
        //EndDAy
        try {

            System.out.println("Enter your EndDate Order");
            Date date = simpleDateFormat.parse(sc.nextLine());//Chuoi thanh Date
            order.setEndOrder(date);//nhan gia tri Date ko nhan String

        } catch (ParseException e) {
            System.err.println("you entered the wrong format ");
            sc.nextLine();

        }
        if (order.getEndOrder()==null){

            orderMenu.orderMenu();
        }
        System.out.println("Status:(1.Not yet,2.Done,3.Deleted)");
        int status = sc.nextInt();
        sc.nextLine();
        order.setStatus(status);
        System.out.println("Set Total for Cart:");
        int total = sc.nextInt();
        sc.nextLine();
        order.setTotalMoney(total);


            if (orderModel.save(order)) {
                orderModel.checkInput();
            } else {
                System.out.println("FALSE");
            }




    }

    public void showList() {
        ArrayList<Order> list = orderModel.findAll();
        System.out.printf("%10s%10s%10s | %10s%10s%10s | %10s%30s%10s | %10s%10s%10s | %11s%5s%7s | %3s%5s%6s| %3s%5s%3s\n",
                "", "OrderId", "",
                "", "UserName", "",
                "", "Products", "",
                "", "TotalMoney", "",
                "", "Status", "",
                "","Enđate", "",
                "","StartDate", "");
        for (int i = 0; i < list.size(); i++) {

            System.out.println(list.get(i).toString() + "     |    " + DateTimeUtil.formatdateToString(list.get(i).getEndOrder()) + "     |    " + DateTimeUtil.formatdateToString(list.get(i).getStartOrder()));

        }
    }

    public void checkInput() {
    }

    public void searchById() {
        System.out.println("Enter your ID you need search: ");
        String orderid = sc.nextLine();
        Order found = orderModel.searchById(orderid);
        if (found != null) {
            System.out.println(found.toString());
        } else {
            System.out.println("Does not exist \n");
        }
    }

    public void TotalByStatus() {
        orderModel.setTotalMoney(0);
        System.out.println("order status you need to search ");
        int so = sc.nextInt();
        System.out.println("Total by Status: " + orderModel.TotalByStatus(so));
        sc.nextLine();
        sc.nextLine();


    }

    public void TotalByDate() {
        orderModel.setTotalMoneyDate(0);
        System.out.println("please enter your Date you need search " +
                "(The date you entered must be greater than the EndOrderDate and you must enter correct format ): ");
        String date = sc.nextLine();
        if (orderModel.TotalByDateYear(date) <= -1) {

            System.out.println("Total money by date: " + orderModel.getTotalMoneyDate());
        } else {
            System.out.println("before EndOrderDate or you entered the wrong format ");
        }

    }


}
