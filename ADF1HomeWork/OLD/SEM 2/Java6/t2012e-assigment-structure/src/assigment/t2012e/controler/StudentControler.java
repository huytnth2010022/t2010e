package assigment.t2012e.controler;

import assigment.t2012e.entity.Student;
import assigment.t2012e.model.StudentModel;

import java.util.ArrayList;
import java.util.Scanner;

public class StudentControler {
    //viết hàm create, yêu cầu người dùng nhập thông tin sv
    //trả về thông tin sinh viên
    Scanner sc = new Scanner(System.in);

    StudentModel studentModel = new StudentModel();

    public void create() {

        Student student = new Student();
        System.out.println("please enter rollNumber: ");
        String rollNumber = sc.nextLine();
        student.setRollNumber(rollNumber);
        System.out.println("please enter Full name: ");
        String fullName = sc.nextLine();
        student.setFullName(fullName);
        System.out.println("please enter email: ");
        String email = sc.nextLine();
        student.setEmail(email);
        System.out.println("please enter phone: ");
        String phone = sc.nextLine();
        student.setPhone(phone);
        if (studentModel.save(student)) {
            System.out.println("Action success");
        } else {
            System.out.println("Action false");
        }


    }

//    public void SoLuong() {
//        System.out.println("nhập vào số lượng sinh viên cần nhập vào hệ thống:");
//        int soLuong = sc.nextInt();
//        sc.nextLine();
//        for (int i = 0; i < soLuong; i++) {
//            create();
//
//        }
//        System.out.println("Nhập thông tin sinh viên cần tìm");
//        String rollNumber = sc.nextLine();
//        int flex = -1;
//        for (int i = 0; i < list.size(); i++) {
//            Student student = list.get(i);
//            if (student.getRollNumber().equals(rollNumber)) {
//                System.out.println(list.get(i).toString());
//                flex = 1;
//
//            }
//        }
//        if (flex == -1){
//            System.err.println("(づ￣ 3￣)づ không tìm thấy");
//        }
//
//
//
//    }

    public void showList() {
        ArrayList<Student> list = studentModel.findAll();
        System.out.printf("%10s%10s%10s | %10s%15s%10s | %5s%20s%5s | %5s%10s%5s\n",
                "", "rollNumber", "",
                "", "fullName", "",
                "", "email", "",
                "", "phone", "");
        for (int i = 0; i < list.size(); i++) {

            System.out.println(list.get(i).toString());
        }


    }

    public void search() {
        System.out.println("please enter your rollNumber if you need search ");
        String rollNumber = sc.nextLine();
        Student student = studentModel.findById(rollNumber);
        if (student == null) {
            System.out.println("ko tìm thấy");
        } else {
            System.out.printf("tìm thấy sinh viên với thông tin: %s\n", student.toString());
        }

    }

    public Student delete() {
        System.out.println("please enter your rollNumber if you need delete: ");
        String rollNumber = sc.nextLine();
        Student student = studentModel.findById(rollNumber);
        if (student == null) {
            System.out.println("ko tìm thấy");
        } else {
            System.out.printf("tìm thấy sinh viên với thông tin: %s\n", student.toString());
        }
        System.out.println("if you want to delete:(Y/N)");
        String choice = sc.nextLine();
        if (choice.equalsIgnoreCase("y")) {
            if (studentModel.delete(rollNumber)) {
                System.out.println("delete successful");
            }
        } else {
            System.out.println("delete false");
        }


        return null;

    }

    public void update() {
        System.out.println("Please fill in rollNumber to update");
        String rollNumber = sc.nextLine();
        Student student = studentModel.findById(rollNumber);
        if (student == null) {
            System.out.println("ko tìm thấy");
        } else {
            System.out.printf("tìm thấy sinh viên với thông tin: %s\n", student.toString());


            Student updateStudent = new Student();

            System.out.println("update Student full name");
            String fullName = sc.nextLine();
            updateStudent.setFullName(fullName);
            System.out.println("update Student email");
            String email = sc.nextLine();
            updateStudent.setEmail(email);
            System.out.println("update Student phone");
            String phone = sc.nextLine();
            updateStudent.setPhone(phone);

            if (studentModel.update(rollNumber, updateStudent)) {
                System.out.println("😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻😻");
                showList();
            } else {
                System.out.println("💔💔💔💔💔");
            }

        }
    }
}
