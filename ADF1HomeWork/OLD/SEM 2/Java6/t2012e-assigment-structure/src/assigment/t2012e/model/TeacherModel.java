package assigment.t2012e.model;

import assigment.t2012e.entity.Teacher;

import java.util.ArrayList;

public class TeacherModel {
    private ArrayList<Teacher> list = new ArrayList<>();

    public boolean addTeacher(Teacher obj) {
        list.add(obj);
        return true;
    }

    public ArrayList<Teacher> findAllTeacher() {
        return list;
    }

    public Teacher findTeacher(String id) {
        Teacher obj = null;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getTeacherId().equals(id)) {
                obj = list.get(i);
            }
        }
        return obj;
    }

    public boolean updateTeacher(String id, Teacher updateobj) {
        Teacher existingTeacher = findTeacher(id);
        if (existingTeacher == null) {
            return false;
        }
        existingTeacher.setName(updateobj.getName());
        existingTeacher.setPhone(updateobj.getPhone());
        existingTeacher.setSubjects(updateobj.getSubjects());
        return true;

    }

    public boolean deleteTeacher(String id) {
        Teacher existingTeacher = findTeacher(id);
        if (existingTeacher == null) {
            return false;
        }
        list.remove(existingTeacher);
        return true;
    }
}
