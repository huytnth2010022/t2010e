package assigment.t2012e.entity;

import java.util.ArrayList;

public class Teacher {
    private String TeacherId;
    private String Name;
    private  String phone;
    private String subjects;

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public String toString(){
        return String.format("%10s%10s%10s | %10s%15s%10s | %5s%20s%5s | %5s%10s%5s \n ",
                "",TeacherId,"",
                "",Name,"",
                "",phone,"",
                "",subjects,"");
    }
    public Teacher(){

    }

    public Teacher(String teacherId, String name, String phone, String subjects) {
        TeacherId = teacherId;
        Name = name;
        this.phone = phone;
        this.subjects = subjects;
    }

    public String getTeacherId() {
        return TeacherId;
    }

    public void setTeacherId(String teacherId) {
        TeacherId = teacherId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }




}
