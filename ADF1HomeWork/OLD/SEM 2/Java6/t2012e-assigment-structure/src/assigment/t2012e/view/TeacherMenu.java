package assigment.t2012e.view;


import assigment.t2012e.controler.TeacherControler;

import java.util.Scanner;

public class TeacherMenu {
 public  void getMenuTeacher(){
     TeacherControler teacherControler = new TeacherControler();
     Menu menu = new Menu();

     Scanner scanner = new Scanner(System.in);

     while(true){
         System.out.println("Teacher manager 🙄🙄🙄");
         System.out.println("----------------");
         System.out.println("1.Create new");
         System.out.println("2.Show list");
         System.out.println("3.Update");
         System.out.println("4.Delete");
         System.out.println("5.Back");
         System.out.println("0.Exit");
         System.out.println("please Enter your choice:");
         int choice = scanner.nextInt();
         scanner.nextLine();
         switch(choice){
             case 1:
                 teacherControler.createTeacher();
                 break;
             case 2:
                 teacherControler.showList();
                 break;
             case 3:
                 teacherControler.updateTeacher();
                 break;
             case 4:
                 teacherControler.delete();
                 break;
             case 5:
                 menu.GetMenu();
                 break;
             case 0:
                 System.out.println("nothing here!!");
                 break;
             default:
                 break;
         }
         if (choice==0){
             break;
         }



     }
 }

}
