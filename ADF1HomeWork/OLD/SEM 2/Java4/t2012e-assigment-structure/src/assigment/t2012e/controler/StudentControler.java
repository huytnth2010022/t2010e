package assigment.t2012e.controler;

import assigment.t2012e.entity.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class StudentControler {
    //viết hàm create, yêu cầu người dùng nhập thông tin sv
    //trả về thông tin sinh viên
    Scanner sc = new Scanner(System.in);
    ArrayList<Student> list = new ArrayList<>();

    public Student create() {

        Student student = new Student();
        System.out.println("please enter rollNumber: ");
        String rollNumber = sc.nextLine();
        student.setRollNumber(rollNumber);
        System.out.println("please enter Full name: ");
        String fullName = sc.nextLine();
        student.setFullName(fullName);
        System.out.println("please enter email: ");
        String email = sc.nextLine();
        student.setEmail(email);
        System.out.println("please enter phone: ");
        String phone = sc.nextLine();
        student.setPhone(phone);
        list.add(student);
        return student;
    }

    public void showList() {
        System.out.printf("%10s%10s%10s | %10s%15s%10s | %5s%20s%5s | %5s%10s%5s\n",
                "", "rollNumber", "",
                "", "fullName", "",
                "", "email", "",
                "", "phone", "");
        for (int i = 0; i < list.size(); i++) {

            System.out.println(list.get(i).toString());
        }
        String next = sc.nextLine();

    }

    public Student search() {
        System.out.println("please enter your rollNumber if you need search ");
        String rollNumber = sc.nextLine();
        for (int i = 0; i < list.size(); i++) {
            Student student = list.get(i);
            if (student.getRollNumber().equals(rollNumber)) {
                return student;

            }
        }
        return null;
    }

    public Student delete() {
        System.out.println("please enter your rollNumber if you need delete: ");
        String rollNumber = sc.nextLine();
        for (int i = 0; i < list.size(); i++) {
            Student student = list.get(i);
            if (student.getRollNumber().equals(rollNumber)) {
                System.err.println("Do you want to delete this student? y/n");
                String choice = sc.nextLine();
                switch (choice) {

                    case "y":
                    list.remove(i);
                        System.out.println("complete delete ");
                        String complete = sc.nextLine();
                    break;
                    case "n":
                        break;
                    default:
                        System.out.println("nothing");
                        break;
                }

            }

        }
        return null;

    }
    public  Student update(){
        System.out.println("Please fill in rollNumber to update");
        String rollNumber = sc.nextLine();
        for (int i = 0; i < list.size(); i++) {
            Student student = list.get(i);
            if (student.getRollNumber().equals(rollNumber)) {
                System.out.println("update Student full name");
                String fullName = sc.nextLine();
                student.setFullName(fullName);
                System.out.println("update Student email");
                String email = sc.nextLine();
                student.setEmail(email);
                System.out.println("update Student phone");
                String phone = sc.nextLine();
                student.setPhone(phone);
                list.set(i,student);
                showList();
                return student;




            }

        }
        return null;


    }

}
