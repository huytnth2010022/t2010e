package assigment.t2012e.view;

import assigment.t2012e.controler.StudentControler;

import java.util.Scanner;

public class StudentMenu {
  public void getStudentMenu(){
      StudentControler studentControler = new StudentControler();

      Scanner scanner = new Scanner(System.in);

      while(true){
          System.out.println("Student manager");
          System.out.println("----------------");
          System.out.println("1.Create new");
          System.out.println("2.Show list");
          System.out.println("3.Update");
          System.out.println("4.Delete");
          System.out.println("0.Exit");
          System.out.println("please Enter your choice:");
          int choice = scanner.nextInt();
          scanner.nextLine();
          switch(choice){
              case 1:
                  studentControler.create();
                  break;
              case 2:
                  studentControler.showList();
                  break;
              case 3:
                  studentControler.update();
                  break;
              case 4:
                  studentControler.delete();
                  break;
              case 0:
                  System.out.println("nothing here!!");
                  break;
              default:
                  break;
          }
          if (choice==0){
              break;
          }



      }

  }
}
