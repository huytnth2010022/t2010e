package Battery;
/*
Viết mô tả về bóng đèn tại đây.
@author (Trinh Ngoc Huy)
* @version 1.0 13/6/2021
* */
public class Battery {
    private int energy;
    public Battery(){
        energy = 100;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int value) {
        this.energy = value;
    }
    public void decreaseEnergy(){
        energy--;
    }
}
