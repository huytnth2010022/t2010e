package Temperature;

public class Weather {
    public static void main(String[] args) {
        Temperature temp = new Temperature(25.00);
        System.out.println("nhiệt độ: " + temp.getcTemp() + "°C");
        System.out.println("nhiệt độ (°F): " + temp.getFTemp() + "°F");
        System.out.println("nhiệt độ (K): " + temp.getKTemp() + "K");
    }
}
