package Temperature;

public class Temperature {
    private double cTemp;

     Temperature(double cTemp) {
        this.cTemp = cTemp;
    }

     double getcTemp() {
        return cTemp;
    }

     void setcTemp(double cTemp) {
        this.cTemp = cTemp;
    }
    double getFTemp(){
         double f = cTemp * 1.8000 + 32.00;
         return f;
    }
    double getKTemp() {
        double k = cTemp + 273.15;
        return k;
    }
}
