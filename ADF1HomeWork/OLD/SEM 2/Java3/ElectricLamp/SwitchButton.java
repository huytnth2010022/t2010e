package ElectricLamp;

public class SwitchButton {
    private  boolean status;
    private ElectricLamp lamp;

    public SwitchButton() {
        status = false;

    }
    void connecToLamp(ElectricLamp lamp){
this.lamp = lamp;

    }
    void switchOn(){
        lamp.turnOn();
        status = true;

    }
    void switchOff(){
        lamp.turnOff();
        status = false;
    }

    public boolean displayStatus() {
        return status;
    }
}
