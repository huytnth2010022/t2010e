package StudentManagementSofware;

public class Client {
    public static void main(String[] args) {
        Student st1  = new Student();

        Student st2 = new Student(1,"Nguyen Van A",true);
        st2.printInfo();
        st2.setName("Nguyen Van B");
        st1.setName("Huy");
        st2.printInfo();
        st1.printInfo();
    }
}
