public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        car.accelerate();
        Car c = new LuxuryCar();
        SportCar sportCar = new SportCar();
        sportCar.accelerate();
        c.accelerate();

           System.out.println(new SportCar().equals(sportCar));


        // c.service Đây là đối tượng Car không phải LuxuryCar mặc dù được khai báo là class LuxuryCar
    }
}
