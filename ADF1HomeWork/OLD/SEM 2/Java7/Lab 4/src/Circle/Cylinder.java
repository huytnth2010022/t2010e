package Circle;

public class Cylinder extends Circle {
    private double height;


    public Cylinder() {
    this.height = 10.00;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "Cylinder{" +
                "height=" + height +
                '}';
    }
    public double getArea(){
        double area = super.getArea();
        return area;
    }
    public double getVolume(){
        return this.height*getArea();
    }
}
