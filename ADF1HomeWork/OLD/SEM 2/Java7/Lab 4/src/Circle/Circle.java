package Circle;

public class Circle {
    private double radius;
    private String color;

    public Circle() {
        this.color = "Red";
        this.radius = 24.60;
    }

    public double getRadius() {
        return radius;
    }

    public String getColor() {
        return color;
    }
    public double getArea(){
        return radius*radius*3.14;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                '}';
    }
}
