funcitonal areas involved:
Lễ Tân 
Khoa hồ sơ bệnh nhân
Phòng hành chính

Hardware requirement
VD: mục tiêu là cải tạo lại hệ thống sao cho nhanh gọn,dễ sử dụng
ngân sách khoảng 500 triệu
đối tượng: bệnh nhân

software requirement 
bạn muốn trực quan hóa dữ liệu theo cách thức nào
bạn có muốn áp dụng những kỹ thuật phân tích thống kê 
bạn có muốn chúng tôi lược bỏ bớt đi các thủ tục không cần thiết
bạn cần quản lý dữ liệu thông tin như thế nào
(khách hàng muốn gì?)

Data input:
dữ liệu bệnh nhân
Data output:


System functions:
chức năng đăng ký
chức năng đăng nhập
Phân quyền, bảo mật
chức năng nhập,xuất dũ liệu

Performance:
hệ thống phải nhanh, chuyển giữ các trang phải trơn chu
Security:
hệ thống phải có các bước bảo mật thông qua việc login,xác minh danh tính đầy đủ,và có các
biện pháp kịp thời khi xảy ra biến cố (bị người dùng lạ xâm nhập trái phép thông qua tài
 khoản của nhân viên,quên tài khoản login)
users:
có thể nhập thông tin bệnh nhân vào hệ thống và chia sẻ thông tin đó với các khu vực khác để sử lý dữ liệu
pattients:
truy xuất dữ liệu bệnh án của chính bệnh nhân đó,xem các thông báo từ bệnh viện
link to orther information systems:
(Cho link báo sức khỏe)

Rich picture:
là một phương pháp luận hệ thống mềm
các hình ảnh phong phú cung cấp về các vấn đề phức tạp hoặc chưa được xác định rõ = cách vẽ chi tiết, không theo cú pháp nhất định 
đòi hỏi người sáng tạo phải suy nghĩ sâu sắc về vấn đề và hiểu

SSADM
Lợi ích:
+đảm bảo các bước đều được thực hiện
+dễ dàng đo lường tiến độ bằng cách tham khảo các mục tiêu được xác định cho tường bước 
+đảm bảo việc lập kế hoạch và lập trình kỹ lưỡng
Bất lợi:
+Thiếu tính linh hoạt
+sự tham gia của người dùng bị hạn chế
+Tốn nhiều thời gian hơn so với các phương pháp cho phép các giai đoạn được lặp lại

RAD/RSD
http://www.selectbs.com/analysis-and-design/what-is-rapid-application-development
Lợi ich:
+Tăng tốc độ phát triển và tăng chất lượng
(Bằng cách  tạo mẫu nhanh, ảo hóa các quy trình liên quan đến hệ thống, sử dụng các công cụ CASE và các kỹ thuật khác)
(Chắt lượng là đáp ứng nhu cầu của người dùng cũng như hệ thống phân phỗi có chi phí bảo trì thấp)
( RAD làm tăng chất lượng thông qua sự tham gia của người dùng vào các giai đoạn phân tích và thiết kế)
Bất lợi:
+ giảm khả năng mở rộng
(sảy ra khi ứng dụng được phát triển RAD bắt đầu như một nguyên mẫu và phát triển thành một ứng dụng hoàn thiện)
+giảm tính năng
( các tính năng được đẩy lên các phiên bản sau để hoàn thành bản phát hành trong một khoảng thời gian ngắn)

Các Bước thực hiện:
https://quanlydoanhnghiep.edu.vn/phuong-phap-phat-trien-ung-dung-nhanh-rapid-application-development/
  Bước 1: Lập kế hoạch
  Bước 2: Thiết kế hệ thống
   Bước 3: Phát triển hệ thống
   Bước 4: Vận hành hệ thống
DFD:
Data Flows:

Data Source:

Processes:

Storage:
Kỹ thuật phân mức trong DFD

Căn cứ vào việc phân rã chức năng của một BFD:
- Mô tả một DFD theo nhiều mức khác nhau.
- Mỗi mức được thể hiện trong một hoặc nhiều trang.
- Mức 0: còn gọi là mức bối cảnh
- Chỉ gồm một DFD, trong đó chỉ có một chức năng
duy nhất (chức năng tổng quát của hệ thống) trao
đổi các luồng thông tin với các tác nhân ngoài.
- Tên của trang mức 0 là tên của hệ thống.
- Mức 1: còn gọi là mức đỉnh
- Chỉ gồm một DFD
Các mức 2,3,4,... mỗi mức gồm nhiều DFD được
thành lập như sau:
- Cứ mỗi chức năng ở mức trên, ta thành lập một
DFD ở mức dưới, gọi là biểu diễn DFD ở mức con.
- Phân rã chức năng đó thành nhiều chức năng con
- Vẽ lại các luồng dữ liệu vào và ra chức năng trên,
nhưng bây giờ phải vào hoặc ra chức năng con thích
hợp.
- Nghiên cứu các quan hệ về dữ liệu giữa các chức
năng con, nhờ đó bổ sung các luồng dữ liệu nội bộ
hoặc các kho dữ liệu nội bộ.
- Các chức năng được đánh số theo ký pháp chấm
(.) để tiện theo dõi vệt triển khai từ trên xuống.
https://thinhnotes.com/chuyen-nghe-ba/use-case-diagram-va-5-sai-lam-thuong-gap/

UseCase:
1.Giới Thiệu:
+ US mô tả tập các hoạt động hoạt động của hệ thống theo quan điểm các
Actor(người tham gia hệ thống) Nó mô tả các yêu cầu của hệ thống và trả
lời các câu hỏi:"Hệ thống phải làm gì ?"
+ Một US có thể bao gồm nhiều biểu đồ US khác
+ US mô tả rõ ràng và nhất quán về việc hệ thống cần phải làm gì
+ US mô tả về các yêu cầu về mặt chức năng của hệ thống các chức năng 
này có từ sự thỏa thuận giữa khách hàng và nhóm phát triển phần mềm

2.Mục tiêu của Use Case:
-là cơ sở để:
 + người phân tích viên hiểu
 + người thiết kế xây dượng các kiến trúc
 + người kiểm thử kiểm tra các kết quả thực hiện của hệ thống
 + người lập trình cài đặt các kết quả thực hiện của hệ thống
- Làm công cụ giao tiếp cho những người phát triển, đảm bảo hệ thống thỏa 
mãn đúng những yêu cầu của người dùng
3.Các thành phần trong US
-Hệ thống
+hệ thống là một thành phần của US nên ranh giới của hệ thống
cần phải được xác định rõ ràng
+ một hệ thống không nhất thiết là một hệ thống phần mềm, nó có thể là một chiếc
máy hoặc một doanh nghiệp
+ký hiệu của hệ thống hình chữ nhật có kèm theo tên hệ thống
-Actor
+người hoặc một vật nào đó tương tác với hệ thống,sử dụng hệ thống
+một tác nhân có thể là người, thiết bị mà cũng có thể là một hệ thống khác
+tên gọi của tác nhân được mô tả bawgfn các danh từ(chung) và
thường phải đặc tả được vai trò của nó đối với hệ thống
+môt tác nhân là một dạng tập thực thể(một lớp) chứ không phải một thực thể
Tác nhân mô rả và đại diện cho một vai trò, chứ không phảu một người sử dụng
 thật sự và cụ thể của hệ thống US
+các chức năng cần có tùy theo các tác nhân Actor
4.Cách xác định các tác nhân
= Ai sẽ sử dụng các chức năng chính của hệ thống?
= Ai cần sự hỗ trợ của hệ thống để thực hiện các công việc hằng ngày?
= ai quản trị, bảo dưỡng để đảm bảo cho hệ thống hoạt động thường xuyên?
= hệ thống quản lý sử dụng những thiết bị nào?
= hệ thống cần tương tác với những hệ thống nào khác
= ai hay cái gì quan tâm đến kết quả xử lý của hệ thống
5.Xác định các Use Case:
-xác định Use Case dựa vào các tác nhân.
+Xác định những tác nhân liên quan đến hệ thống hoặc đến tổ chức
nghĩa là tìm và xác định những tác nhân là người sử dụng hay những hệ thống
khác tương tác với hệ thống cần xây dựng
+với mỗi tác nhân, tìm những tiến trình (chức năng) được khởi đầu 
hay giúp các tác nhân thực hiện, giao tiếp tương tác với hệ thống
-Xác định các US và các sự kiện
+Xác định những sự kiện bên ngoài các tác động đến hệ thống hay hệ thống phải
trả lời
+tìm mối liên quan giữa các sự kiện và các UC
+hãy trả lời các câu hỏi để tìm ra các UC
=nhiệm vụ chính của các tác nhân là gì?
=tác nhân cần phải đọc,ghi,sửa đổi,cập nhật hay lưu trữ thông tin hay không
=những thay đổi bên ngoài hệ thống thì tác nhân có cần phải thông báo cho hệ 
thống không?
=Những tác nhân nào cần phải được thông báo về những thay đổi của hệ thống?
Hệ thống cần có những đầu vào/ra nào?từ đâu đến đâu