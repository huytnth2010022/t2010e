UK City Hospital is a large hospital located in the south east of England. 
The hospital uses a computer network which links the reception and the Administration Department.
 When a patient is admitted to the hospital he or she is asked to complete a paper-based form with
 his or her personal details and any medical data.This form is taken with the patient to the ward 
that they will be staying in and the patient’s details are read and copied onto a chart that is stored
 at the bottom of the patient’s bed. The information on this chart is updated several times a day.
 If the patient has been in the hospital before, a member of the nursing staff has to retrieve the 
patient’s folder (paper-based) from the Patients’ Records Department to read the details contained in
 it and to check, for example, that they do not suffer from any allergies to medication they may be given to them.
 The patient’s chart is updated with any relevant information. The form is then taken to the
 Administration Department for a copy to be taken of it and the details are entered into the patient
 records database by data-entry staff. If the patient has not been in the hospital before, a new record is created 
for them; if they have been admitted before, their previous record is updated. The patient records database is 
updated once a day. The original form is then taken and placed in the patient’s folder in the Patients’ Records Department.
 When the patient leaves the hospital, their patient chart is taken to the Administration Department where 
the data-entry staff enter information from it into the patient records database. The patient’s chart is then taken
 to be stored in the patient’s folder in the Patients’ Records Department. The manager of the hospital is 
very concerned about the current information system and has met with various senior staff and it has been decided 
that a new information system is required.