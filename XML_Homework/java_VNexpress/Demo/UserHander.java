package Demo;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

public class UserHander extends DefaultHandler {
    ArrayList<TamSu> arrayList = new ArrayList<>();
    TamSu tamSu;
    boolean isTitle;
    boolean isDescription;
    boolean isPubDate;
    boolean isLink;
    boolean isGuid;
    boolean isComments;
boolean isItem;
    public ArrayList<TamSu> getArrayList() {
        return arrayList;
    }
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("item")){
            tamSu = new TamSu();
            isItem = true;
        }
        if (isItem){
          if (qName.equals("title")){
                isTitle = true;
            }else if (qName.equals("description")){
                isDescription = true;
            }
          else if (qName.equals("pubDate")){
                isPubDate = true;
            }
          else if (qName.equals("link")){
                isLink = true;
            }else if (qName.equals("guid")){
                isGuid = true;
            }else if (qName.equals("slash:comments")){
                isComments = true;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        if (qName.equals("item")){
            arrayList.add(tamSu);
            isItem = false;
        } else if (qName.equals("title")){
            isTitle = false;
        }else if (qName.equals("description")){
            isDescription = false;
        }else if (qName.equals("pubDate")){
            isPubDate = false;
        }else if (qName.equals("link")){
            isLink = false;
        }else if (qName.equals("guid")){
            isGuid = false;
        }else if (qName.equals("slash:comments")){
            isComments = false;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch,start,length);
        if (isTitle){
            tamSu.setTitle(value);
        }else if (isDescription){
            tamSu.setDescription(new String(ch,start,length));
        }else if (isPubDate){
            tamSu.setPubDate(value);
        }else if (isLink){
            tamSu.setLink(new String(ch,start,length));
        }else if (isGuid){
            tamSu.setGuid(new String(ch,start,length));
        }else if (isComments){
            tamSu.setComments(Integer.parseInt(new String(ch,start,length)));
        }
    }
}
