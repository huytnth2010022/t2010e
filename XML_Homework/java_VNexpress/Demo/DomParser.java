package Demo;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;

public class DomParser {
    public static ArrayList<TamSu> readVNexpress() throws ParserConfigurationException {
        ArrayList<TamSu> arrayList = new ArrayList<>();
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse("https://vnexpress.net/rss/tam-su.rss");
            Element element = document.getDocumentElement();
            NodeList listChanel = element.getElementsByTagName("item");
            for (int i = 0; i < listChanel.getLength(); i++) {
                TamSu tamSu = new TamSu();
                Node Chanel = listChanel.item(i);
                NodeList listChildChanel = Chanel.getChildNodes();
                for (int j = 0; j < listChildChanel.getLength(); j++) {
                    Node childChanel = listChildChanel.item(j);
                    if (childChanel.getNodeType() != Node.ELEMENT_NODE) {
                        continue;
                    } else {
                        switch (childChanel.getNodeName()) {
                            case "title":
                                tamSu.setTitle(childChanel.getTextContent());
                                break;
                            case "description":
                                tamSu.setDescription(childChanel.getTextContent());
                                break;
                            case "pubDate":
                                tamSu.setPubDate(childChanel.getTextContent());
                                break;
                            case "link":
                                tamSu.setLink(childChanel.getTextContent());
                                break;
                            case "guid":
                                tamSu.setGuid(childChanel.getTextContent());
                                break;
                            case "slash:comments":
                                tamSu.setComments(Integer.parseInt(childChanel.getTextContent()));
                            break;


                        }
                    }
                }
arrayList.add(tamSu);
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
return arrayList;
    }

}

