package Demo;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.ArrayList;
public class SaxParser {

    public static ArrayList<TamSu> readVNexpress() {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            UserHander userHander = new UserHander();
            saxParser.parse("https://vnexpress.net/rss/tam-su.rss", userHander);
            return userHander.getArrayList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
