package Demo;


import java.sql.*;
import java.util.ArrayList;

public class VNxpressDB {
    public void insertDB () throws SQLException {
        ArrayList<TamSu> arrayList = SaxParser.readVNexpress();
Connection connection = Conection.getConnection();
        for (int i = 0; i < arrayList.size(); i++) {
            TamSu tamSu = arrayList.get(i);
            PreparedStatement preparedStatement = connection.prepareStatement("insert into tamsu values(?,?,?,?,?)");
            preparedStatement.setString(1,tamSu.getTitle());
            preparedStatement.setString(2,tamSu.getDescription());
            preparedStatement.setString(3,tamSu.getLink());
            preparedStatement.setString(4,tamSu.getGuid());
            preparedStatement.setInt(5,tamSu.getComments());
            preparedStatement.execute();
        }

    }
}
