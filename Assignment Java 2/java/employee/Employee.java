package employee;

import java.util.Calendar;
import java.util.Date;
import DateUtil.Dateformat;
@Table(name = "employees")
public class Employee {
    private String name;
    private String address;
    private String email;
    @ID()
    private String account;
    private String password;
    private Date creatAt;
    private Date updatedate;
    private int status;
    public Employee() {
            this.creatAt = Calendar.getInstance().getTime();
            this.updatedate = Calendar.getInstance().getTime();
        this.status = 1;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreatAt() {
        return Dateformat.formatDateToString(creatAt);
    }

    public void setCreatAt(Date creatAt) {
        this.creatAt = creatAt;
    }

    public String getUpdate() {
        return Dateformat.formatDateToString(updatedate);
    }

    public void setUpdate(Date update) {
        this.updatedate = update;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
