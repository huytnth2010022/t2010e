package employee;

import ConnectionHelper.ConnectionHelper;
import DateUtil.Dateformat;
import org.reflections.Reflections;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.Calendar;
import java.util.Scanner;
import java.util.Set;

public class EmployeeModel {


    Scanner sc = new Scanner(System.in);

    public void register() {
        Reflections reflections = new Reflections("employee");
        Set<Class<?>> clazzs = reflections.getTypesAnnotatedWith(Table.class);
        for (Class<?> clazz : clazzs) {
            try {
                Table table = (Table) clazz.getAnnotation(Table.class);
                if (table.name().equalsIgnoreCase("employees")) {
                    migrateData(clazz);
                    Connection cnn = ConnectionHelper.getConnection();
                    try (PreparedStatement preparedStatement = cnn.prepareStatement("INSERT INTO ? VALUES (?,?,?,?,?,?,?,?)")) {
                        Field[] fields = clazz.getDeclaredFields();
                        preparedStatement.setString(1, table.name());
                        for (Field field : fields) {
                            int count = 2;
                            if (field.getType().getSimpleName().equalsIgnoreCase("String")) {
                                System.out.println("please enter " + field.getName() + " :");
                                String a = sc.nextLine();
                                preparedStatement.setString(count, a);
                                count++;
                            } else if (field.getType().getSimpleName().equalsIgnoreCase("Date")) {
                                String date = Dateformat.formatDateToString(Calendar.getInstance().getTime());
                                preparedStatement.setString(count, date);
                                count++;
                            } else if (field.getType().getSimpleName().equalsIgnoreCase("int")) {
                                System.out.println("please enter " + field.getName() + " :");
                                int a = sc.nextInt();
                                preparedStatement.setInt(count, a);
                                count++;
                            }

                        }

                        System.out.println(preparedStatement);
                        preparedStatement.execute();
                    } catch (SQLException ex1) {
                        ex1.printStackTrace();
                    }
                }

            } catch (Exception ex) {
                if (ex.getMessage().equalsIgnoreCase("Table 'employees' already exists")) {
                    //insert
                    Connection cnn = ConnectionHelper.getConnection();
                    try (Statement statement = cnn.createStatement()) {
                        StringBuilder stringBuilder = new StringBuilder();
                        Field[] fields = clazz.getDeclaredFields();
                        Table table = (Table) clazz.getAnnotation(Table.class);
                        stringBuilder.append("INSERT INTO ");
                        stringBuilder.append(table.name());
                        stringBuilder.append(" VALUES(");
                        for (Field field : fields) {
                            if (field.getType().getSimpleName().equalsIgnoreCase("String")) {
                                System.out.println("please enter " + field.getName() + " :");
                                String a = sc.nextLine();
                                String content = "'" + a + "'";
                                stringBuilder.append(content);
                                stringBuilder.append(", ");
                            } else if (field.getType().getSimpleName().equalsIgnoreCase("Date")) {
                                String date = Dateformat.formatDateToString(Calendar.getInstance().getTime());
                                String content = "'" + date + "'";
                                stringBuilder.append(content);
                                stringBuilder.append(", ");
                            } else if (field.getType().getSimpleName().equalsIgnoreCase("int")) {
                                System.out.println("please enter " + field.getName() + " :");
                                int a = sc.nextInt();
                                stringBuilder.append(a);
                                stringBuilder.append(", ");
                            }

                        }
                        stringBuilder.setLength(stringBuilder.length() - 2);
                        stringBuilder.append(")");
                        System.out.println(stringBuilder.toString());
                        statement.execute(stringBuilder.toString());
                    } catch (SQLException ex1) {
                        ex1.printStackTrace();
                    }
                } else {
                    System.out.println("Create table fails!!");
                }
            }
        }

    }

    public boolean checkExistAccount(String accountexist,String pwd) {
        Connection connection = ConnectionHelper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM employees ");
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                if (accountexist.equals(rs.getString(4))&& pwd.equals(rs.getString(5))) {
                    return true;
                }
            }
            return false;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return false;
    }
    public boolean login(){

        System.out.println("please enter your account: ");
        String accountexist = sc.nextLine();
        System.out.println("please enter your password: ");
        String psw = sc.nextLine();
        if (checkExistAccount(accountexist,psw)){
            System.out.println("Logged in successfully😁 \n");
            return true ;
        }
        else {
            System.out.println("Login failed ☹\n");
            return false;
        }

    }


    private static void migrateData(Class clazz) throws SQLException, IllegalAccessException {
        String tableName = clazz.getSimpleName();//default = class Name


        Table table = (Table) clazz.getAnnotation(Table.class);
        if (table.name() != null && !table.name().isEmpty()) {

            tableName = table.name();
        }

        StringBuilder sqlQueryBuilder = new StringBuilder();
        sqlQueryBuilder.append("CREATE TABLE");
        sqlQueryBuilder.append(" ");
        sqlQueryBuilder.append(tableName);
        sqlQueryBuilder.append(" ");
        sqlQueryBuilder.append("(");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            sqlQueryBuilder.append(field.getName());
            sqlQueryBuilder.append(" ");
            boolean idPrimaryKey = field.isAnnotationPresent(ID.class);
            if (field.getType().getSimpleName().equalsIgnoreCase("int")) {
                sqlQueryBuilder.append("INT ");
            } else if (field.getType().getSimpleName().equalsIgnoreCase("String")) {
                sqlQueryBuilder.append("VARCHAR (200) ");
            } else if (field.getType().getSimpleName().equalsIgnoreCase("Date")) {
                sqlQueryBuilder.append("DATE ");
            } else if (field.getType().getSimpleName().equalsIgnoreCase("double")) {
                sqlQueryBuilder.append("DOUBLE ");
            }
            if (idPrimaryKey) {
                ID id = field.getAnnotation(ID.class);
                sqlQueryBuilder.append("PRIMARY KEY");
            }
            sqlQueryBuilder.append(", ");
        }
        sqlQueryBuilder.setLength(sqlQueryBuilder.length() - 2);
        sqlQueryBuilder.append(")");
        Connection cnn = ConnectionHelper.getConnection();
        Statement stt = cnn.createStatement();
//        System.out.println(sqlQueryBuilder.toString());
        stt.execute(sqlQueryBuilder.toString());

    }

}
