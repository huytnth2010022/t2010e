package ConnectionHelper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionHelper {
    private static final String Database_url = "jdbc:mysql://localhost:3306/%s";
    private static final String Database_name = "human_resource";
    private static final String Database_user = "root";
    private static final String Database_pwd = "";
    private static Connection cnn;
    public static Connection getConnection() {
        try {
            if (cnn == null || cnn.isClosed()){
                cnn = DriverManager.getConnection(String.format(Database_url,Database_name),Database_user,Database_pwd);
            }
            return cnn;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }
}
