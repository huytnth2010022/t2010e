package DateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Dateformat {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static Date pareDateFromString(String strDatetime){
        try {
            return simpleDateFormat.parse(strDatetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Calendar.getInstance().getTime();
    }
    public static String formatDateToString(Date date){
        return simpleDateFormat.format(date);
    }
}
