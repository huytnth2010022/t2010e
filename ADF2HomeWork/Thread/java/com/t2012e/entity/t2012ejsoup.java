package com.t2012e.entity;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;


public class t2012ejsoup {


    public static void main(String[] args) throws IOException {
        ArrayList<Article> list = new ArrayList<>();

       Document doc = (Document) Jsoup.connect("https://vnexpress.net/the-thao").get();
      Elements elements = doc.select(".title-news a");
       for (int i = 0; i < elements.size(); i++) {
           Article article = new Article();
           article.setUrl(elements.get(i).attr("href"));
           article.setStatus(0);
           list.add(article);
          System.out.println(elements.get(i).attr("href"));

      }
        System.out.println(list.size());

        for (int i = 0; i < elements.size(); i++) {
            Article article = list.get(i);
            Document currentDoc = Jsoup.connect(article.getUrl()).get();
           String title = currentDoc.select("h1.title-detail,div.section-inner").first().text();
           String content = currentDoc.select("article.fck_detail,div.section-inner").first().text();
           article.setTitle(title);
        article.setContent(content);
         insertArticle(article);
           System.out.printf("%d - %s\n",i+1,title);
     }

        }
    public static void insertArticle(Article article) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/articles", "root", "");
            PreparedStatement preparedStatement = connection.prepareStatement("insert into articles(url,title,description,content,thumbnail,createAt,updateAt,status) values (?,?,?,?,?,?,?,?) ");

            preparedStatement.setString(1, article.getUrl());
            preparedStatement.setString(2, article.getTitle());
            preparedStatement.setString(3, article.getDescription());
            preparedStatement.setString(4, article.getContent());
            preparedStatement.setString(5, article.getThumbnail());
            preparedStatement.setString(6, article.getDobString());
            preparedStatement.setString(7, article.getUpdateAtString());
            preparedStatement.setInt(8, article.getStatus());
            preparedStatement.execute();


        } catch (SQLException exception) {
            exception.printStackTrace();
        }

    }
    }

