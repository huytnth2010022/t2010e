package com.t2012e.entity;

import com.t2012e.fptacademy.util.DateTimeUtil;

import java.util.Calendar;
import java.util.Date;

public class Article {
    private String url;
    private String title;
    private String description;
    private String content;
    private String thumbnail;
    private Date createAt;
    private Date updateAt;
    private int status;//0 chưa hoàn thiện 1 đã hoàn thiện -1 đã xóa

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Article() {
        this.createAt = Calendar.getInstance().getTime();
        this.updateAt = Calendar.getInstance().getTime();
        this.status = 0;
        this.title = "";
        this.description = "";
        this.content = "";
        this.thumbnail = "";
    }

    public Article(String url, String title, String description, String content, String thumbnail, Date createAt, Date updateAt, int status) {
        this.url = url;
        this.title = title;
        this.description = description;
        this.content = content;
        this.thumbnail = thumbnail;
        this.createAt = Calendar.getInstance().getTime();
        this.updateAt = Calendar.getInstance().getTime();
        this.status = status;
    }

    public String getDobString() {
        return DateTimeUtil.formatDateToString(this.createAt);
    }

    public String getUpdateAtString() {
        String b = DateTimeUtil.formatDateToString(this.updateAt);
        return b;
    }

    public boolean isVoid() {
if (this.title.length()==0){
    System.err.println("title is required");
    return  false;
}
if(this.content.length()==0){
    System.err.println("content is required");
    return  false;
}
        return true;
    }
}
