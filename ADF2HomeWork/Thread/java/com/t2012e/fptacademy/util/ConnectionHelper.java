
package com.t2012e.fptacademy.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionHelper {
    private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/%s";
    private static final String DATABASE_NAME = "articles";
    private static final String DATABASE_USER = "root";
    private static final String DATABASE_PWD = "";
    private static Connection cnn;



    public static Connection getConnection() throws SQLException {
        if (cnn == null || cnn.isClosed()) {
            cnn = DriverManager.getConnection(String.format("jdbc:mysql://localhost:3306/%s", "articles"), "root", "");
        }

        return cnn;
    }
}
