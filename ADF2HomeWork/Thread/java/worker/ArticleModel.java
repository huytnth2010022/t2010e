package worker;

import com.t2012e.fptacademy.util.ConnectionHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ArticleModel {





    public boolean insertArticle(com.t2012e.entity.Article article) {
        try {
            Connection cnn = ConnectionHelper.getConnection();
            PreparedStatement preparedStatement = cnn.prepareStatement("insert into articles(url,title,description,content,thumbnail,createAt,updateAt,status) values (?,?,?,?,?,?,?,?) ");

            preparedStatement.setString(1, article.getUrl());
            preparedStatement.setString(2, article.getTitle());
            preparedStatement.setString(3, article.getDescription());
            preparedStatement.setString(4, article.getContent());
            preparedStatement.setString(5, article.getThumbnail());
            preparedStatement.setString(6, article.getDobString());
            preparedStatement.setString(7, article.getUpdateAtString());
            preparedStatement.setInt(8, article.getStatus());
            preparedStatement.execute();
            return true;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return false;
    }
}
