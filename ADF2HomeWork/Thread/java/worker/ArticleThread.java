package worker;

import com.t2012e.entity.Article;
import com.t2012e.fptacademy.util.DateTimeUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.print.Doc;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
public class ArticleThread extends Thread {
    Article article;
    private String url;

    public ArticleThread(String url) {
        this.url = url;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public ArticleThread() {
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void run() {
        System.out.printf("Crowing data from url %s\n", url);
        crowData();

    }


    private void crowData() {
        try {
            article = new Article();
            article.setUrl(url);
            Document document = Jsoup.connect(url).get();
            //Lấy tiêu đề
            Element titleNode = document.selectFirst("h1.title-detail");
            if (titleNode != null) {
                String title = titleNode.text();
                article.setTitle(title);
            }
            Element descriptionElement = document.selectFirst("p.description");
            if (descriptionElement != null) {
                String description = descriptionElement.text();
                article.setDescription(description);
            }
            Elements contentElement = document.select("p.Normal");
            for (int i = 0; i < contentElement.size(); i++) {
                if (contentElement != null) {
                    String content = contentElement.text();
                    article.setContent(content);
                }
                else{
                    article.setContent("null");
                }
            }
            Element thumbnailElement = document.selectFirst("div.fig-picture picture img");
            if (thumbnailElement != null) {
                String thumbnail = thumbnailElement.attr("data-src");
                article.setThumbnail(thumbnail);
            } else {
                article.setThumbnail("http://default.jpeg");
            }
            article.setStatus(1);

        } catch (IOException ioException) {
            ioException.printStackTrace();
            System.err.printf("Error %s ", ioException.getMessage());
        }
    }


}

