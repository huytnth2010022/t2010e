package HashMap;
import DateUtil.DateTimeUtil;
import java.util.Date;

public class Teacher {
    private String name;
    private String id;
    private Date dab;
    private int cake;

    public Teacher() {
    }

    public Teacher(String name, String id, int cake) {
        this.name = name;
        this.id = id;
        this.cake = cake;
    }

    public Teacher(String name, String id, String dab, int cake) {
        this.name = name;
        this.id = id;
        this.dab = DateTimeUtil.pareDateFromString(dab);
        this.cake = cake;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", cake=" + cake +
                '}';
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDabString() {
        return DateTimeUtil.formatDateToSing(this.dab);
    }
    public Date getDab() {
        return this.dab;
    }

    public void setDab(Date dab) {
        this.dab = dab;
    }

    public int getCake() {
        return cake;
    }

    public void setCake(int cake) {
        this.cake = cake;
    }
}
