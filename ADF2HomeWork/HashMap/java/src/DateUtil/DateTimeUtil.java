package DateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    public static Date pareDateFromString(String strDatetim){
        try {
            return simpleDateFormat.parse(strDatetim);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Calendar.getInstance().getTime();
    }
public static String formatDateToSing(Date date){
        return simpleDateFormat.format(date);
}
}
