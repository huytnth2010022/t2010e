package refklection;

import java.lang.annotation.Target;

@Table(name = "nhanvien")
public class Employee {
    @ID(autoIncrement = true)
    private int id;
    private String name;
    private String phone;
    private double blance;

    public Employee() {
    }

    public Employee(int id, String name, String phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
