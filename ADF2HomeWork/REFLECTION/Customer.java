package refklection;
@Table(name = "khachhang")
public class Customer {
    @ID(autoIncrement = true)
    private int id;
    private String name;
    private String phone;
    private String email;
    private String adress;

    public Customer() {
    }

    public Customer(int id, String name, String phone, String email, String adress) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.adress = adress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}
