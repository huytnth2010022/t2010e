package refklection;

import com.t2012e.fptacademy.util.ConnectionHelper;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DemoReflection {
    public static void main(String[] args) throws SQLException, IllegalAccessException {
        Employee employee = new Employee(12,"Hà Háo Sắc","0916166161");
        Customer customer = new Customer(90,"Hiếu","091212344","hieu@gmail.com","HaNoi");
      Model model = new Model();
      model.findAll(employee);
  model.delete(customer);
      model.update(employee);

    }

    private static void migrateData(Class clazz) throws SQLException, IllegalAccessException {
        String tableName = clazz.getSimpleName();//default = class Name
        if (clazz.isAnnotationPresent(Table.class)){

       Table table = (Table) clazz.getAnnotation(Table.class);
if (table.name() != null && !table.name().isEmpty()){

    tableName = table.name();
}
        }
        StringBuilder sqlQueryBuilder = new StringBuilder();
        sqlQueryBuilder.append("CREATE TABLE");
        sqlQueryBuilder.append(" ");
        sqlQueryBuilder.append(tableName);
        sqlQueryBuilder.append(" ");
        sqlQueryBuilder.append("(");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field:fields) {
            sqlQueryBuilder.append(field.getName());
            sqlQueryBuilder.append(" ");
            boolean idPrimaryKey =field.isAnnotationPresent(ID.class);
            if (field.getType().getSimpleName().equalsIgnoreCase("int")){
                sqlQueryBuilder.append("INT ");
            }
            else if (field.getType().getSimpleName().equalsIgnoreCase("String")){
                sqlQueryBuilder.append("VARCHAR (200) ");
            }
            else if (field.getType().getSimpleName().equalsIgnoreCase("double")){
                sqlQueryBuilder.append("DOUBLE ");
            }
            if (idPrimaryKey) {
                ID id = field.getAnnotation(ID.class);
                if (id.autoIncrement()){
                    sqlQueryBuilder.append("AUTO_INCREMENT ");
                }
                sqlQueryBuilder.append("PRIMARY KEY");
            }
            sqlQueryBuilder.append(", ");
        }
        sqlQueryBuilder.setLength(sqlQueryBuilder.length() -2);
        sqlQueryBuilder.append(")");
        Connection cnn = ConnectionHelper.getConnection();
        Statement stt= cnn.createStatement();
        System.out.println(sqlQueryBuilder.toString());
        stt.execute(sqlQueryBuilder.toString());

    }


}
