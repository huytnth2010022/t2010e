package refklection;

import com.t2012e.fptacademy.util.ConnectionHelper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class Model<T> {
    Scanner sc = new Scanner(System.in);
    public void findAll(T obj)  {
        ArrayList<T> list = new ArrayList<>();
        Connection cnn = null;
        try {
            Class clazz = obj.getClass();
            String tableName = clazz.getSimpleName();
            if (clazz.isAnnotationPresent(Table.class)){
               Table table = (Table) clazz.getAnnotation(Table.class);
                if (table.name()!=null && !table.name().isEmpty() ){
                    tableName = table.name();
                }
            }

            Field[] fields = clazz.getDeclaredFields();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("SELECT");
            stringBuilder.append(" ");
            stringBuilder.append("*");
            stringBuilder.append(" ");
            stringBuilder.append("FROM ");
            stringBuilder.append(tableName);
            cnn = ConnectionHelper.getConnection();
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(stringBuilder.toString());
         while (rs.next()){
             for (int i = 0; i < fields.length; i++) {
                 if (fields[i].getType().getSimpleName().equals("int")){
                     System.out.println(rs.getInt(fields[i].getName()));
                 }else if (fields[i].getType().getSimpleName().equals("String")){
                     System.out.println(rs.getString(fields[i].getName()));
                 }else if (fields[i].getType().getSimpleName().equals("double")){
                     System.out.println(rs.getDouble(fields[i].getName()));
                 }
             }
             System.out.println("______________________________________");
         }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
    public void delete(T obj){
        Class clazz = obj.getClass();
        String tableName = clazz.getSimpleName();
        if (clazz.isAnnotationPresent(Table.class)){
            Table table = (Table) clazz.getAnnotation(Table.class);
        if (table.name()!=null && !table.name().isEmpty()){
            tableName = table.name();
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DELETE FROM ");
        stringBuilder.append(tableName);
        stringBuilder.append(" ");
        stringBuilder.append("WHERE ");
    Field[] fields = clazz.getDeclaredFields();

            for (Field field:fields) {
                boolean idPrimaryKey =field.isAnnotationPresent(ID.class);
                if (idPrimaryKey){
                    stringBuilder.append(field.getName());
                    stringBuilder.append(" = ");
                    if (field.getType().getSimpleName().equalsIgnoreCase("String")){
                        stringBuilder.append("'");
                        System.out.println("Nhập ID bạn muốn xóa");
                        String id = sc.nextLine();
                        stringBuilder.append(id);
                        stringBuilder.append("';");
                    }else if (field.getType().getSimpleName().equalsIgnoreCase("int")){
                        System.out.println("Nhập ID bạn muốn xóa");
                        int id = sc.nextInt();
                        stringBuilder.append(id);
                        stringBuilder.append(";");
                    }
                }
            }
            try {
                Connection cnn = ConnectionHelper.getConnection();
                Statement statement = cnn.createStatement();
                statement.execute(stringBuilder.toString());
                System.out.println(stringBuilder.toString());
            } catch (SQLException exception) {
                exception.printStackTrace();
            }


        }

    }
    public void update(T obj){
        Class clazz = obj.getClass();
        String tableName = clazz.getSimpleName();
        if (clazz.isAnnotationPresent(Table.class)){
            Table table = (Table) clazz.getAnnotation(Table.class);
            if (table.name()!=null&&!table.name().isEmpty()){
                tableName =table.name();
            }

        }
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ");
        sql.append(tableName);
        sql.append(" ");
        sql.append("SET ");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field:fields) {
           boolean primarykey = field.isAnnotationPresent(ID.class);

            if (primarykey){
            }
            else {
                if (field.getType().getSimpleName().equalsIgnoreCase("int")){
                    sql.append(field.getName());
                    sql.append(" = ");
                    System.out.println("nhập " + field.getName()+" mới:");
                    int newinfor = sc.nextInt();
                    sql.append(newinfor);
                    sql.append(", ");

                }else   if (field.getType().getSimpleName().equalsIgnoreCase("String")){
                    sql.append(field.getName());
                    sql.append(" = '");
                    System.out.println("nhập " + field.getName()+" mới:");
                   String newinfor = sc.nextLine();
                    sql.append(newinfor);
                    sql.append("' , ");

                }else   if (field.getType().getSimpleName().equalsIgnoreCase("double")){
                    sql.append(field.getName());
                    sql.append(" = ");
                    System.out.println("nhập " + field.getName()+" mới:");
                    double newinfor = sc.nextDouble();
                    sql.append(newinfor);
                    sql.append(", ");

                }
            }
        }
        sql.setLength(sql.length()-2);
        sql.append(" WHERE ");
        for (Field field:fields) {
            boolean primarykey = field.isAnnotationPresent(ID.class);
            if (primarykey){
                sql.append(field.getName());
                if (field.getType().getSimpleName().equalsIgnoreCase("int")){
                    sql.append("=");
                    System.out.println("nhập " + field.getName()+"cần đổi");
                    int id = sc.nextInt();
                    sql.append(id);
                }else if (field.getType().getSimpleName().equalsIgnoreCase("String")){
                    sql.append("= '");
                    System.out.println("nhập " + field.getName()+"cần đổi");
                   String id = sc.nextLine();
                    sql.append(id);
                    sql.append("'");
                }
            }
        }
        sql.append(";");

        try {
            Connection cnn = ConnectionHelper.getConnection();
            Statement statement = cnn.createStatement();
            statement.execute(sql.toString());
            System.out.println(sql.toString());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

    }
    public void save(T obj)  {
       try{
           Class clazz = obj.getClass();// lấy thông tin lớp 'Class' từ đối tượng
           // 1. Kết nối đến database.
           Connection cnn = ConnectionHelper.getConnection();
           Statement stt = cnn.createStatement();
           String tableName = clazz.getSimpleName();
           if (clazz.isAnnotationPresent(Table.class)){
               Table table = (Table) clazz.getAnnotation(Table.class);
               if (table.name() != null && !table.name().isEmpty()){
                   tableName = table.name();
               }
           }
           // sinh ra chuỗi gồm danh sách các trường cần insert vào db,
           // ví dụ: (identityNumber, name, balance, email)
           String fieldNames = "(";
           Field[] fields = clazz.getDeclaredFields();
           for (int i = 0; i < fields.length; i++) {
               fieldNames += fields[i].getName() + ", ";
           }
           // nhanh trí xoá dấu , cuối cùng (và khoảng trắng) trước khi thêm dấu đóng ngoặc đơn.
           fieldNames = fieldNames.substring(0, fieldNames.length() - 2);
           fieldNames += ")";
           // sinh ra chuỗi gồm danh sách các giá trị tương ứng của trường
           // kèm theo việc xử lý nếu kiểu dữ liệu là String thì thêm dấu '
           // Lưu ý, cần bổ sung các kiểu dữ liệu khác nhau để sinh ra dấu ' hoặc không có.
           // Ví dụ: ('A001', 'Hung', 0, 'hung@gmail.com')
           String fieldValues = "(";
           for (int i = 0; i < fields.length; i++) {
               fields[i].setAccessible(true);
               if (fields[i].getType().getSimpleName().equals("String")) {
                   fieldValues += "'" + fields[i].get(obj) + "', ";
               } else {
                   fieldValues += fields[i].get(obj) + ",";
               }
           }
           // nhanh trí xoá dấu , cuối cùng (và khoảng trắng) trước khi thêm dấu đóng ngoặc đơn.
           fieldValues = fieldValues.substring(0, fieldValues.length() - 2);
           fieldValues += ")";
           // 2. Tạo ra câu lệnh truy vấn từ các phần ở trên kèm theo các lệnh insert và values
           // để tạo ra một câu lệnh như sau:
           // 'insert into Customer (identityNumber, name, balance, email) values ('A001', 'Hung', 0, 'hung@gmail.com')'
           String sqlQuery = String.format("insert into %s %s values %s", tableName, fieldNames, fieldValues);
           System.out.println(sqlQuery);
           // 3. Gửi câu lệnh vào database.
           stt.execute(sqlQuery);
       }catch (SQLException | IllegalAccessException ex){
           ex.printStackTrace();
       }
    }


}
